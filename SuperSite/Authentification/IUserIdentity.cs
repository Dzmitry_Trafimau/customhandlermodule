﻿using System.Security.Principal;

namespace SuperSite.Authentification
{
    interface IUserIdentity : IIdentity
    {
        User User { get; set; }
        void Init(string login);
    }
}
