﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace SuperSite.Authentification
{
    public interface IAuthentication
    {
        HttpContext HttpContext { get; set; }
        User TryLogin(string userName, string password, bool isPersistent=true);

        void Logout();

        IPrincipal CurrentUser { get; }
    }
}