﻿using Ninject;
using SuperSite.Bindings;
using SuperSite.Repository;

namespace SuperSite.Authentification
{
    public class UserIndentity : IUserIdentity
    {
        public User User { get; set; }
        private IRepository repository; 

        public string AuthenticationType
        {
            get
            {
                return typeof(User).ToString();
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return User != null;
            }
        }

        public string Name
        {
            get
            {
                return User != null ? User.UserName : "Anonym";
            }
        }

        public UserIndentity()
        {
            repository = CreatorObjects.GetRepository();
        }

        public void Init(string login)
        {
            if (!string.IsNullOrEmpty(login))
            {
                User = repository.Search(_=>_.UserName==login);
            }
        }
    }
}