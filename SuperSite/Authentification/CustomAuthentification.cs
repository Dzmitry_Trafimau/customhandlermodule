﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Ninject;
using SuperSite.Repository;

namespace SuperSite.Authentification
{
    public  class CustomAuthentication : IAuthentication
    {

        private const string CookieName = "supersite_authentification_cookie";

        public  HttpContext HttpContext { get; set; }
        private  IPrincipal _currentUser;
        [Inject]
        public  IRepository Repository { get; set; }

        public  User TryLogin(string userName, string password, bool isPersistent=true)
        {
            User retUser = Repository.Search(_ => _.UserName == userName && _.Password == password);

            if (retUser != null)
            {
                CreateCookie(userName, isPersistent);
            }
            return retUser;
        }

        private void CreateCookie(string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                  1,
                  userName,
                  DateTime.Now,
                  DateTime.Now.Add(FormsAuthentication.Timeout),
                  isPersistent,
                  string.Empty,
                  FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            var encTicket = FormsAuthentication.Encrypt(ticket);

            // Create the cookie.
            var authCookie = new HttpCookie(CookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            try
            {
                HttpContext.Response.Cookies.Set(authCookie);
            }
            catch(Exception e)
            {
                var a = e.Message;
            }
           
        }

        public void Logout()
        {
            var httpCookie = HttpContext.Response.Cookies[CookieName];
            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
                _currentUser=new UserProvider(null);
            }
        }

        public IPrincipal CurrentUser
        {
            get
            {
                if (_currentUser == null || _currentUser.Identity.Name == "Anonym")
                {
                    try
                    {
                        HttpCookie authCookie = HttpContext.Request.Cookies.Get(CookieName);
                        if (authCookie != null && !string.IsNullOrEmpty(authCookie.Value))
                        {
                            var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                            _currentUser = new UserProvider(ticket.Name);
                        }
                        else
                        {
                            _currentUser = new UserProvider(null);
                        }
                    }
                    catch (Exception ex)
                    {
                        _currentUser = new UserProvider(null);
                    }
                }
                return _currentUser;
            }
        }
    }
}