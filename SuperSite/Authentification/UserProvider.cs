﻿using System.Security.Principal;

namespace SuperSite.Authentification
{
    public class UserProvider : IPrincipal
    {
        private IUserIdentity _userIdentity;

        #region IPrincipal Members

        public IIdentity Identity
        {
            get { return _userIdentity; }
        }

        public bool IsInRole(string role)
        {
            return _userIdentity.User != null;
        }

        #endregion


        public UserProvider(string name)
        {
            _userIdentity = new UserIndentity();
            _userIdentity.Init(name);
        }


        public override string ToString()
        {
            return _userIdentity.Name;
        }
    }
}