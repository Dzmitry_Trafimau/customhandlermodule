﻿using System;
using System.Reflection;
using System.Web;
using Ninject;
using SuperSite.Bindings;

namespace SuperSite.Authentification
{
    public class AuthentificationModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += Authenticate;
            //context.EndRequest += ResetCurrentUser;
        }

        private void Authenticate(Object source, EventArgs e)
        {
            HttpApplication app = (HttpApplication)source;
            HttpContext context = app.Context;
            IAuthentication auth = CreatorObjects.GetAuthentication();
            auth.HttpContext = context;
            context.User =auth.CurrentUser;
        }

        //public void ResetCurrentUser(Object source, EventArgs e)
        //{
        //    HttpApplication app = (HttpApplication)source;
        //    HttpContext context = app.Context;
        //    context.User = null;
        //}
        public void Dispose()
        {
        }
    }
}