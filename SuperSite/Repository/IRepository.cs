﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperSite.Repository
{
    public interface IRepository
    {
        User Create(User entity);
        User Search(Func<User, bool> predicate);
    }
}