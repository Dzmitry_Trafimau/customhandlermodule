﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SuperSite.Repository
{
    public class Repository:IRepository
    {


        public User Create(User entity)
        {
            using (var motoEntities = new MotoEntities())
            {
                motoEntities.Set<User>().Add(entity);
                motoEntities.SaveChanges();
                return entity;
            }
        }

        public User Search(Func<User, bool> predicate)
        {
            using (var motoEntities = new MotoEntities())
            {
                return motoEntities.Set<User>().FirstOrDefault(predicate);
            }
        }
    }
}