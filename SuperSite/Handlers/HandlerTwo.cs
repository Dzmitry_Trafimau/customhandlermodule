﻿using System.Reflection;
using System.Web;
using Ninject;
using SuperSite.Authentification;
using SuperSite.Repository;

namespace SuperSite.Handlers
{
    public class HandlerTwo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                //if (HttpContext.Current.User.Identity.IsAuthenticated)
                    context.Response.WriteFile("~/Motocycle.json");
            }
            catch
            {

            }
        }

        public bool IsReusable { get; private set; }
    }
}