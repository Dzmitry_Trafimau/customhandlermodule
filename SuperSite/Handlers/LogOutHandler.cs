﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Ninject;
using SuperSite.Authentification;
using SuperSite.Bindings;

namespace SuperSite.Handlers
{
    public class LogOutHandler:IHttpHandler
    {
         private IAuthentication _authentication;

         public LogOutHandler()
         {
             _authentication = CreatorObjects.GetAuthentication();

         }
        public void ProcessRequest(HttpContext context)
        {
            _authentication.Logout();
        }

        public bool IsReusable { get; private set; }
    }
}