﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Ninject;
using SuperSite.Authentification;
using SuperSite.Bindings;

namespace SuperSite.Handlers
{
    public class AuthorizeHandler:IHttpHandler
    {
        private IAuthentication _authentication;

        public AuthorizeHandler()
        {
            _authentication = CreatorObjects.GetAuthentication();
        }
        public void ProcessRequest(HttpContext context)
        {
            var userName = context.Request.Params["userName"];
            var password = context.Request.Params["password"];
            var user2 = _authentication.TryLogin(userName,password);
            context.Response.ContentType = "text/plain";
            var textResult = user2!=null ?  "yes" : "no";
            context.Response.Write(textResult);
        }

        public bool IsReusable { get; private set; }
    }
}