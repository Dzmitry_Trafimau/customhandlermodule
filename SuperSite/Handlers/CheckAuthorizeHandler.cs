﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperSite.Handlers
{
    public class CheckAuthorizeHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var curuser = context.User;
            context.Response.Write(HttpContext.Current.User.Identity.IsAuthenticated ? "yes" : "no");
        }

        public bool IsReusable { get; private set; }
    }
}