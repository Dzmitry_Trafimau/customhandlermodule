var Get = Get || {};
Get.$ = function (id) {
    return document.getElementById(id);
}
Get.byClass = function (className) {
    return document.getElementsByClassName(className);
}

Get.targetElement=function (event){
    return  event.target || event.srcElement;
}



var ManagerElements = ManagerElements || {};
ManagerElements.SetLayoutPaneles = function (maincontenState, authentificationState, loginState, logoutState, load, errorMessage) {
    var re = /(^|\s)(on|off)(\s|$)/;
    var elements = Get.byClass("main_content");
    ManagerElements.replaceClassesInElements(elements, re, maincontenState);

    elements = Get.byClass("authentification_panel");
    ManagerElements.replaceClassesInElements(elements, re, authentificationState);

    elements = Get.byClass("main_menu_item_logout");
    ManagerElements.replaceClassesInElements(elements, re, logoutState);

    var element = Get.$("loginone");
    ManagerElements.replaceClassesInAlement(element, re, loginState);

    element = Get.$("load");
    ManagerElements.replaceClassesInAlement(element, re, load);

    element = Get.$("errorMessage");
    ManagerElements.replaceClassesInAlement(element, re, errorMessage);
}
ManagerElements.clickWidgetTab = function (targetElement) {
    ManagerElements.unactivateAllWidgetTabs();
    ManagerElements.unactivatedAllWidgetPanels();
    targetElement.className = targetElement.className + " selected";
    ManagerElements.activateWidgetTabe(targetElement);
}
ManagerElements.activateWidgetTabe = function (targetElementTab) {
    var re = /(^|\s)(on|off)(\s|$)/;
    var tabpanelId = targetElementTab.id;
    var panel = "";
    if (tabpanelId == "menu_tabs_jsonstring")
        panel = Get.$("jsonstring");
    else
        panel = Get.$("parsedjson");
    panel.className = panel.className.replace(re, " on ");

}
ManagerElements.unactivateAllWidgetTabs = function () {
    var element = Get.$("menu_tabs_jsonstring");
    var re = /(^|\s)(selected)(\s|$)/;
    element.className = element.className.replace(re, " ");
    element = Get.$("menu_tabs_parsedjson");
    element.className = element.className.replace(re, " ");
}
ManagerElements.unactivatedAllWidgetPanels = function () {
    var re = /(^|\s)(on|off)(\s|$)/;
    var elements = Get.byClass("menu_content_panel");
    for (var i = 0; i < elements.length; i++) {
        elements[i].className = elements[i].className.replace(re, " off ");
    }
}
ManagerElements.lastAuthPanelState = "off";
ManagerElements.onAuthentificationPanel = function () {
    if (ManagerElements.lastAuthPanelState == "on") {
        ManagerElements.SetLayoutPaneles("off", "off", "on", "off", "off","off");
        ManagerElements.lastAuthPanelState = "off";
    } else {
        ManagerElements.SetLayoutPaneles("off", "on", "on", "off", "off","off");
        ManagerElements.lastAuthPanelState = "on";
    }
}
ManagerElements.loadButtonClick = function () {
    ManagerElements.SetLayoutPaneles("on", "off", "off", "on", "off","off");
}
ManagerElements.replaceClassesInElements=function (elements, replacedclasses, newclass) {
    for (var i = 0; i < elements.length; i++) {
        ManagerElements.replaceClassesInAlement(elements[i], replacedclasses, newclass);
    }
}
ManagerElements.replaceClassesInAlement=function (element, replacedclasses, newclass) {
    element.className = element.className.replace(replacedclasses, " " + newclass + " ");
}
ManagerElements.hasClass=function (elem, className) {
    return new RegExp("(^|\\s)" + className + "(\\s|$)").test(elem.className);
}



var JSONOperation = JSONOperation || {};
JSONOperation.createHTML = function (jsonstring) {
    var item = [];
    var subHtml = "";
    for (var i = 0; i < jsonstring.length; i++) {
        var innerTemplate = "";
        var a = Object.keys(jsonstring[i]).length;
        for (var j = 0; j < a; j++) {
            innerTemplate += JSONOperation.setItem(jsonstring[i], j);
        }
        subHtml += "<li class=\"Node IsRoot ExpandOpen\">" +
            "<div class=\"Expand\"></div>" +
            "<div class=\"Content\">{}</div>" +
            "<ul class=\"Container\">" +
            innerTemplate +
            "</ul></li>";
    }
    var outtemplate = "<ul class=\"Container\">" + subHtml + "</ul>";
    item.push(outtemplate);
    return item.join('');
}
JSONOperation.setItem = function (jsonElemetn, numberofelemet) {
    var key = Object.keys(jsonElemetn)[numberofelemet];
    var value = jsonElemetn[key];
    var subElements = "";
    var hasSubElement = false;
    var expand = "ExpandOpen ";
    var stringvalue = "";
    if (value !== Object(value)) {
        expand = "ExpandWithoutChild ";
        stringvalue = value;
    } else {
        var a = Object.keys(value).length;
        for (var _j = 0; _j < a; _j++) {
            subElements += JSONOperation.setItem(value, _j);
        }
        hasSubElement = true;
    }
    var templete = "<li class=\"Node " + expand + "\"> " +
        "<div class=\"Expand\"></div>" +
        "<div class=\"Content\">" + key + ": " + stringvalue + "</div>";
    if (hasSubElement)
        templete += "<ul class=\"Container\"> " + subElements + "</ul>";
    templete += "</li>";
    return templete;
}
JSONOperation.setJSONtoElement = function (id) {
    var x = Get.$(id);
    var url = "~/Handler.json";
    var xnt = new XMLHttpRequest();
    xnt.onreadystatechange = function () {
        if (xnt.readyState == 4 && xnt.status == 200) {
            if (id == "jsonstring")
                x.innerHTML = "<pre>" + xnt.responseText + "</pre>";
            else {
                var arryValues = JSON.parse(xnt.responseText);
                var htm = JSONOperation.createHTML(arryValues);
                x.innerHTML = htm;
            }
        }
    }
    xnt.open("GET", url, true);
    xnt.send();

}
JSONOperation.switchJsonTree=function (event) {
    event = event || window.event;
    var clickedElem = event.target || event.srcElement;

    if (ManagerElements.hasClass(clickedElem, 'Expand')) {
        var node = clickedElem.parentNode;
        if (!ManagerElements.hasClass(node, 'ExpandWithoutChild')) {
            var newClass = ManagerElements.hasClass(node, 'ExpandOpen') ? 'ExpandClosed' : 'ExpandOpen';
            var re = /(^|\s)(ExpandOpen|ExpandClosed)(\s|$)/;
            node.className = node.className.replace(re, '$1' + newClass + '$3');
        }
    }
}


var Authentification = Authentification || {}
Authentification.isAuthorized = function () {
    var result = false;
    var httpReq = new XMLHttpRequest();
    httpReq.open("GET", "CheckAuthorize", false);
    httpReq.onreadystatechange = function () {
        var respons = httpReq.response;
        if (respons == "yes")
            result = true;
        else
            result = false;
    };
    httpReq.send(null);
    return result;
}
Authentification.login = function (idUserName, idpassword) {
    var userName = Get.$(idUserName).value;
    var password = Get.$(idpassword).value;
    var httpReq = new XMLHttpRequest();
    httpReq.open("GET", "Authentifiactation?userName=" + userName + "&password=" + password, false);
    httpReq.onreadystatechange = function () {
        if (httpReq.response == "yes" && Authentification.isAuthorized()) {
            ManagerElements.SetLayoutPaneles("off", "off", "off", "on", "on", "off");
            ManagerElements.lastAuthPanelState = "off";
        } else {
            ManagerElements.SetLayoutPaneles("off", "on", "on", "off", "off","on");
        }
    };
    httpReq.send(null);
}
Authentification.logout = function () {
    var httpReq = new XMLHttpRequest();
    httpReq.open("GET", "Logout", true);
    httpReq.onreadystatechange = function () {
        ManagerElements.SetLayoutPaneles("off", "off", "on", "off", "off","off");
    };
    httpReq.send(null);
}
Authentification.onloadPage = function () {
    var isAuthorised = Authentification.isAuthorized();
    return isAuthorised ? ManagerElements.SetLayoutPaneles("off", "off", "off", "on", "on", "off") : ManagerElements.SetLayoutPaneles("off", "off", "on", "off", "off", "off");
}


Get.$("menu_tabs_jsonstring").addEventListener("click", function (event) {
    var targetElement = Get.targetElement(event);
    ManagerElements.clickWidgetTab(targetElement);
});
Get.$("menu_tabs_jsonstring").addEventListener("click", JSONOperation.setJSONtoElement('jsonstring'));

Get.$("menu_tabs_parsedjson").addEventListener("click", JSONOperation.setJSONtoElement('parsedjson'));

Get.$("menu_tabs_parsedjson").addEventListener("click", function (event) {
    var targetElement = Get.targetElement(event);
    ManagerElements.clickWidgetTab(targetElement);
});

Get.$("parsedjson").addEventListener("click", function () { JSONOperation.switchJsonTree(arguments[0]); });

document.getElementsByTagName("BODY")[0].addEventListener("load", function () {
    Authentification.onloadPage();
});

Get.$("loginone").addEventListener("click", function () {
    ManagerElements.onAuthentificationPanel();
});

Get.$("logout").addEventListener("click", function () {
    Authentification.logout();
});

Get.$("login").addEventListener("click", function () {
    Authentification.login("idusername", "idpassword");
});

Get.$("load").addEventListener("click", function () {
    ManagerElements.loadButtonClick();
});

Get.$("idusername").addEventListener("keyup", function () {
    ManagerElements.SetLayoutPaneles("off", "on", "on", "off", "off", "off");
});

Get.$("idpassword").addEventListener("keyup", function () {
    ManagerElements.SetLayoutPaneles("off", "on", "on", "off", "off", "off");
});