﻿using Ninject.Modules;
using Ninject.Web.Common;
using SuperSite.Authentification;
using SuperSite.Repository;

namespace SuperSite.Bindings
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<MotoEntities>().ToSelf();
            Bind<IRepository>().To<Repository.Repository>();
            Bind<IAuthentication>().To<CustomAuthentication>().InSingletonScope();
        }
    }
}