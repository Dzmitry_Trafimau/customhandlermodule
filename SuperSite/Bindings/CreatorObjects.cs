﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Ninject;
using SuperSite.Authentification;
using SuperSite.Repository;

namespace SuperSite.Bindings
{
    public static class CreatorObjects
    {
        private static readonly IKernel Kernel;
           
        static CreatorObjects()
        {
            Kernel = new StandardKernel();
            Kernel.Load(Assembly.GetExecutingAssembly());
        }

        public static IRepository GetRepository()
        {
            return Kernel.Get<IRepository>();
        }

        public static IAuthentication GetAuthentication()
        {
            return Kernel.Get<IAuthentication>();
        }
    }
}